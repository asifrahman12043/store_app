import * as React from 'react';
import { Button, View } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import Login from "./screen/Authenticate/Login";
import Registration from "./screen/Authenticate/Registration";
import Home from "./screen/Pages/Home"
import Text from "./screen/Pages/Text"
import * as Localization from 'expo-localization';
import { LanguageProvider } from 'react-native-translation'
const locale = Localization.locale

const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <LanguageProvider language={locale}>
      <NavigationContainer>
        <Drawer.Navigator initialRouteName="Home">
          <Drawer.Screen name="Home" component={Home} />
          <Drawer.Screen name="Login" component={Login} />
          <Drawer.Screen name="Registration" component={Registration} />
          <Drawer.Screen name="Text" component={Text} />
        </Drawer.Navigator>
      </NavigationContainer>
    </LanguageProvider>
  );
}
