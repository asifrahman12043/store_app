import React, { Component } from 'react';
import { View, Button } from 'react-native';
import { TransText, TranslationConsumer } from 'react-native-translation'
import {getRedirection} from 'react-native-translation'
import foodRedirector from '../foodRedirector'
import   Data  from '../lang/Data'
// const Data = {
//     "en-US": "Sushi is a traditional Japanese dish of prepared vinegared rice, usually with some sugar and salt, accompanying a variety of ingredients, such as seafood, often raw, and vegetables. Styles of sushi and its presentation vary widely, but the one key ingredient is sushi rice, also referred to as shari, or sumeshi!",
//     "fr-FR": "Le sushi est un plat japonais traditionnel de riz vinaigré préparé, généralement avec du sucre et du sel, accompagnant une variété d'ingrédients, tels que des fruits de mer, souvent crus, et des légumes. Les styles de sushi et sa présentation varient considérablement, mais le seul ingrédient clé est le riz à sushi, également appelé shari ou sumeshi    ",
// }

export default class Text extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const food = getRedirection(foodRedirector)
       
        return (
            <View style={{flex:1,justifyContent:"center", margin:20}}>
                {/* <TranslationConsumer>
                    {({ language, updateLanguage }) => {
                        return (
                        <View style={{ flexDirection:"row" }}>
                            <Button
                                title={"English"}
                                onPress={() => { updateLanguage("en-US") }}/>
                            <Button
                                title={"French"}
                                onPress={() => { updateLanguage("fr-FR") }}
                            />
                        </View>
                        )
                    }}
                </TranslationConsumer> */}
                <TransText dictionary={Data} />
                <TransText dictionary={food} />
            </View>
        );
    }
}
