import React, { Component } from 'react';
import { SimpleLineIcons, AntDesign, Entypo, MaterialIcons } from '@expo/vector-icons';
import { StyleSheet, Text, View, SafeAreaView, ScrollView } from "react-native";
import { Card } from 'react-native-elements'


class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      
    };
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.header}>
          <SimpleLineIcons name="menu" size={17} color="black" />
          <Text style={{ fontSize: 20, fontFamily: "Snell Roundhand" }}>Florist</Text>
          <View style={styles.searchIcon}>
            <AntDesign name="search1" size={17} color="black" style={{ paddingRight: 10 }} />
            <AntDesign name="shoppingcart" size={17} color="black" />
          </View>
        </View>
        <View style={styles.location}>
          <View style={styles.locationInner}>
            <Entypo name="location-pin" size={15} color="black" />
            <Text style={{ fontSize: 12 }}>Los Angeles</Text>
          </View>
          <MaterialIcons name="assistant-direction" size={15} color="black" />
        </View>

        <view>
          <Text style={{ fontSize: 15, color: "pink" }}>Best Pick Up</Text>
          <ScrollView horizontal={true}>
            <Card>
              <Card.Image source={require('../../Images/login.png')}></Card.Image>
              <Card.Title>HELLO WORLD</Card.Title>
              <Text style={{ marginBottom: 10 }}>
                The idea with React Native n.
                </Text>
            </Card>

            <Card>
              <Card.Image source={require('../../Images/login.png')}></Card.Image>
              <Card.Title>HELLO WORLD</Card.Title>
              <Text style={{ marginBottom: 10 }}>
                The idea with React Native n.
                </Text>
            </Card>
            <Card>
              <Card.Image source={require('../../Images/login.png')}></Card.Image>
              <Card.Title>HELLO WORLD</Card.Title>
              <Text style={{ marginBottom: 10 }}>
                The idea with React Native n.
                </Text>
            </Card>
          </ScrollView>
        </view>
        <Text style={{ fontSize: 15, color: "pink" }}>Cateloges</Text>
        <Card>
          <Card.Image source={require('../../Images/login.png')}></Card.Image>
          <Card.Title>HELLO WORLD</Card.Title>
          <Text style={{ marginBottom: 10 }}>
            The idea with React Native n.
                </Text>
        </Card>
        <view>

        </view>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  header: {
    flexDirection: "row",
    paddingHorizontal: 40,
    backgroundColor: "#eaeaea",
    justifyContent: "space-between",
    paddingVertical: 20,
    marginTop: 40,
  },
  location: {
    flexDirection: "row",
    paddingHorizontal: 40,
    backgroundColor: "#ddd",
    justifyContent: "space-between",
    paddingVertical: 5,
    alignItems: "center"
  },
  searchIcon: {
    flexDirection: "row",
  },
  locationInner: {
    flexDirection: "row",
    alignItems: "center",

  }

});


export default Home;
