const enFood = require('./lang/enFood.json')
const frFood = require('./lang/frFood.json')
 
const foodRedirector = {
  "en-US" : enFood,
  "fr-FR" : frFood
}
 
export default foodRedirector