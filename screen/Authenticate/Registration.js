import React from 'react';
import { StyleSheet,SafeAreaView, Text, View, Image, PixelRatio, Radio, TextInput, TouchableOpacity , Button} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as Animatable from 'react-native-animatable';
export default class Registration extends React.Component {

  state = {
    firstName: "",
    lastName: "",
    password: "",
    email: '',
    isvalidfirstName: false,
    isvalidlastName: false,
    isvalidPassword: false,
    errors: {}
  }

  selectedMethod = (event) => {
    // const {eventCount, target, text} = event.nativeEvent;
    console.log("text")
  }



  postData = () => {

    const { isValid, errors } = this.validate();
    if (isValid) {
      this.setState({
        errors: {}
      })

      const storeData = async (value) => {
        try {
          await AsyncStorage.setItem('@storage_Key', this.state.firstName)
        } catch (e) {
          // saving error
        }
      }

      // this.props.navigation.navigate('Login')
    } else {
      this.setState({
        errors: errors
      })
    }
  }


  validate = () => {
    let isValid;
    const errors = {}
    const { firstName, lastName, password, email } = this.state
    if (!firstName) {
      errors.firstName = 'First name required'
    } else if (firstName.length < 4) {
      errors.firstName = 'First name is too short'
    } else if (firstName.length > 10) {
      errors.firstName = 'First name is too Long'
    }

    if (!lastName) {
      errors.lastName = 'Last name required'
    } else if (lastName.length > 8) {
      errors.lastName = 'last name too long'
    } else if (lastName.length < 3) {
      errors.lastName = 'last name is too short'
    }

    const reg = /(?=.*\d)/
    let validReg;
    validReg = reg.test(password)
    if (!password) {
      errors.password = 'Password is required'
    } else if (password.length < 6) {
      errors.password = 'password length min 6 charecter'
    } else if (!validReg) {
      errors.password = 'password should have min 1 number'
    }

    const emailregx = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
    let validEmail;
    validEmail = emailregx.test(email)
    if (!email) {
      errors.email = 'Email is required'
    } else if (!validEmail) {
      errors.email = 'Please enter valid email'
    }



    if (Object.keys(errors).length === 0) {
      isValid = 1;
    } else {
      isValid = 0;
    }
    return {
      errors,
      isValid
      // Object.keys(errors).length = 0
    }
  }

  render() {
    let { errors } = this.state
    console.log("error", errors)
    return (
     
      <View style={styles.container}>
        <Image source={require('../../Images/login.png')} style={styles.imageStyle} />
        <Text style={{ fontSize: 22, fontWeight: 'bold', color: "#0517C1", alignContent: "center", justifyContent: "center" }}> Create Your Account </Text>
        <Text style={{ color: "#1E2565", textAlign: "center", alignItems: "center", justifyContent: "center", padding:8 }}> Please input your valid information</Text>
        <TransText dictionary = {message}/>
        <TranslationConsumer>
            {({ language, updateLanguage }) => {
              return(<Button title={"Change Language"} onPress={() => {updateLanguage("fr-FR")}}/>)
            }}
          </TranslationConsumer>
        <View style={styles.inputView} >
          <View style={{ flexDirection: "row", justifyContent: "flex-start" }}>
            <Image style={{ width: 20, height: 20 ,resizeMode:"contain"}} source={require('../../Images/user.png')} />
            <TextInput
              style={styles.inputText}
              underlineColorAndroid={'#AEB1CA'}
              placeholder="First Name "
              placeholderTextColor="#566573"
              onChangeText={text => this.setState({ firstName: text })}
            />
            <TextInput
              style={styles.inputText}
              underlineColorAndroid={'#AEB1CA'}
              placeholder="Last Name"
              placeholderTextColor="#566573"
              onChangeText={text => this.setState({ lastName: text })} />
          </View>
        </View>

        <View style={{flexDirection:"column"}}>
          {
            errors.firstName ?
              <Animatable.View animation="fadeInLeft" duration={500}>
                <Text style={styles.erromassage}>{errors.firstName} </Text>
              </Animatable.View>
              : null
          }
          {
            errors.lastName ?
              <Animatable.View animation="fadeInRight" duration={500}>
                <Text style={styles.erromassage}>{errors.lastName} </Text>
              </Animatable.View>
              : null
          }
        </View>

        <View style={styles.inputView} >
          <View style={{ flexDirection: "row" }}>
            <Image style={{ width: 20, height: 20 ,resizeMode:"contain"}} source={require('../../Images/email.png')} />
            <TextInput
              style={styles.inputTextFullLine}
              underlineColorAndroid={'#AEB1CA'}
              placeholder="Email"
              placeholderTextColor="#566573"
              onChangeText={text => this.setState({ email: text })} />
          </View>
        </View>
        {
          errors.email ?
            <Animatable.View animation="fadeInLeft" duration={500}>
              <Text style={styles.erromassage}>{errors.email} </Text>
            </Animatable.View>
            : null
        }


        <View style={styles.inputView} >
          <View style={{ flexDirection: "row" }} >
            <Image style={{ width: 20, height: 22, resizeMode:"contain" }} source={require('../../Images/password.png')} />
            <TextInput
              secureTextEntry
              underlineColorAndroid={'#AEB1CA'}
              style={styles.inputTextFullLine}
              placeholder="Password"
              placeholderTextColor="#566573"
              onChangeText={text => this.setState({ password: text })} />
          </View>
        </View>

        {
          errors.password ?
            <Animatable.View animation="fadeInLeft" duration={500}>
              <Text style={styles.erromassage}>{errors.password}</Text>
            </Animatable.View>
            : null

        }

        <Text style={{ fontWeight: "bold", fontSize: 18, width: "80%", paddingBottom: "2%", }}>Register as</Text>
        

        <TouchableOpacity style={styles.loginBtn} onPress={this.postData}>
          <Text style={styles.loginText}   > Register</Text>
        </TouchableOpacity>
        

        <View style={{flexDirection:"row"}}>
          <Text style={{fontSize: 16}}>Already have account ? </Text>
          <Text onPress={()=>this.props.navigation.navigate('Login')} style={{fontWeight: "bold",fontSize: 16}}>Login</Text>
        </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageStyle: {
    height:"40%",
    width:"100%",
    justifyContent: "center",
    alignContent: "center",
    resizeMode:"cover"
  },
  logo: {
    fontWeight: "bold",
    fontSize: 50,
    color: "#fb5b5a",
  },
  inputView: {
    // flexDirection:"row",
    width: "80%",
    backgroundColor: "white",
    height: 28,
    marginTop: 2,
    // marginBottom: 20,
    justifyContent: "center",
    padding: 2
  },
  inputText: {
    height: 40,
    width:"50%",
    bottom: 6,
    paddingLeft: "3%",
    fontSize: 15,
    color: "black",
    // backgroundColor:"black"
  },
  inputTextFullLine:{
    height: 40,
    width:"100%",
    bottom: 6,
    paddingLeft: "3%",
    fontSize: 15,
    color: "black",
  },
  erromassage: {
    color: "#C40606",
    fontSize:14
  },
  forgot: {
    color: "black",
    fontSize: 11
  },
  loginBtn: {
    width: "50%",
    backgroundColor: "#0517C1",
    borderRadius: 25,
    height: "8%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 10,
  },
  loginText: {
    color: "white"
  }
});