import React from 'react';
import { StyleSheet, Text, View, Image, PixelRatio, TextInput, Button, TouchableOpacity } from 'react-native';

import * as Animatable from 'react-native-animatable';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { TransText, TranslationConsumer } from 'react-native-translation'
import {getRedirection} from 'react-native-translation'

export default class Login extends React.Component {

  state = {
    firstName: "",
    lastName: "",
    password: "",
    email: '',
    isvalidfirstName: false,
    isvalidlastName: false,
    isvalidPassword: false,
    role: "jobseeker",
    errors: {}
  }

  selectedMethod = (event) => {
    // const {eventCount, target, text} = event.nativeEvent;
    console.log("text")
  }

  

  postData = () => {

    const { isValid, errors } = this.validate();
    if (isValid) {
      this.setState({
        errors: {}
      })
      this.props.navigation.navigate('Login')
    } else {
      this.setState({
        errors: errors
      })
    }
  }


  validate = () => {
    let isValid;
    const errors = {}
    const { firstName, lastName, password, email } = this.state
    if (!firstName) {
      errors.firstName = 'First name required'
    } else if (firstName.length < 4) {
      errors.firstName = 'First name is too short'
    } else if (firstName.length > 10) {
      errors.firstName = 'First name is too Long'
    }

    if (!lastName) {
      errors.lastName = 'Last name required'
    } else if (lastName.length > 8) {
      errors.lastName = 'last name too long'
    } else if (lastName.length < 3) {
      errors.lastName = 'last name is too short'
    }

    const reg = /(?=.*\d)/
    let validReg;
    validReg = reg.test(password)
    if (!password) {
      errors.password = 'Password is required'
    } else if (password.length < 6) {
      errors.password = 'password length min 6 charecter'
    } else if (!validReg) {
      errors.password = 'password should have min 1 number'
    }

    const emailregx = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
    let validEmail;
    validEmail = emailregx.test(email)
    if (!email) {
      errors.email = 'Email is required'
    } else if (!validEmail) {
      errors.email = 'Please enter valid email'
    }



    if (Object.keys(errors).length === 0) {
      isValid = 1;
    } else {
      isValid = 0;
    }
    return {
      errors,
      isValid
      // Object.keys(errors).length = 0
    }
  }

  render() {
    let { errors } = this.state
    console.log("error", errors)
    return (
      <View style={styles.container}>
        <Image source={require('../../Images/login.png')} style={styles.imageStyle} />
        <TranslationConsumer>
                    {({ language, updateLanguage }) => {
                        return (
                        <View style={{ flexDirection:"row" }}>
                            <Button
                                title={"English"}
                                onPress={() => { updateLanguage("en-US") }}/>
                            <Button
                                title={"French"}
                                onPress={() => { updateLanguage("fr-FR") }}
                            />
                        </View>
                        )
                    }}
                </TranslationConsumer>
        <Text style={{ fontSize:25, fontWeight: 'bold', color: "#0517C1", alignContent: "center", justifyContent: "center" }}> Log In</Text>
        <View style={styles.inputView} >
          <View style={{ flexDirection: "row" }}>
            <Image style={{ width: 36, height: 36 ,resizeMode:"center"}} source={require('../../Images/email.png')} />
            <TextInput
              style={styles.inputText}
              underlineColorAndroid={'#AEB1CA'}
              placeholder="Email Address"
              placeholderTextColor="#566573"
              onChangeText={text => this.setState({ email: text })} />
        
          </View>
        </View>
        {
          errors.email ?
            <Animatable.View animation="fadeInLeft" duration={500}>
              <Text style={styles.erromassage}>{errors.email} </Text>
            </Animatable.View>
            : null
        }


        <View style={styles.inputView} >
          <View style={{ flexDirection: "row" }} >
            <Image style={{ width: 36, height: 36 ,resizeMode:"center"}} source={require('../../Images/password.png')} />
            <TextInput
              secureTextEntry
              underlineColorAndroid={'#AEB1CA'}
              style={styles.inputText}
              placeholder="Password"
              placeholderTextColor="#566573"
              onChangeText={text => this.setState({ password: text })} />
          </View>
        </View>

        {
          errors.password ?
            <Animatable.View animation="fadeInLeft" duration={500}>
              <Text style={styles.erromassage}>{errors.password}</Text>
            </Animatable.View>
            : null

        }

        <TouchableOpacity style={styles.loginBtn} onPress={this.postData}>
          <Text style={styles.loginText}>Login</Text>
        </TouchableOpacity>

        <View style={{flexDirection:"row"}}>
          <Text style={{fontSize: 16}}>Don't have any account ? </Text>
          <Text onPress={()=>this.props.navigation.navigate('Registration')} style={{fontWeight: "bold",fontSize: 16, color:"blue"}}>Register</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageStyle: {
    height:"40%",
    width:"100%",
    justifyContent: "center",
    alignContent: "center",
    resizeMode:"cover"
    // bottom:"2%"
  },
  logo: {
    fontWeight: "bold",
    fontSize: 50,
    color: "#fb5b5a",
  },
  inputView: {
    // flexDirection:"row",
    width: "80%",
    backgroundColor: "white",
    // borderRadius:25,
    height: 35,
    marginTop: 2,
    // marginBottom: 20,
    justifyContent: "center",
    padding: 2
  },
  inputText: {
    height:38,
    bottom: 3,
    paddingLeft: "3%",
    width:"90%",
    fontSize: 15,
    borderColor: '#7a42f4',
  },
  
  erromassage: {
    color: "#C40606"
  },
  forgot: {
    color: "black",
    fontSize: 11
  },
  loginBtn: {
    width: "60%",
    backgroundColor: "#0517C1",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 10,

  },
  loginText: {
    color: "white"
  }
});